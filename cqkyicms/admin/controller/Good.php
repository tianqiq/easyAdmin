<?php
/**
 * 重庆柯一网络有限公司 版权所有
 * 开发团队:柯一网络 柯一CMS项目组
 * 创建时间: 2018/5/13 10:07
 * 联系电话:023-52889123 QQ：563088080
 * 惟一官网：www.cqkyi.com
 */

namespace app\admin\controller;


use app\admin\model\GoodModel;

class Good extends Base
{

    protected $title="商城管理";
    public function index(){

       $name = "商品管理";
       if(request()->isAjax()) {


           $param = input('param.');
           $limit = $param['pageSize'];
           $offset = ($param['pageNumber'] - 1) * $limit;
           $where = [];
           if (!empty($param['searchText'])) {
               $where['good_name'] = ['like', '%' . $param['searchText'] . '%'];
           }
//           if(!empty($param['deptId'])){
//               $where['dept_id']=input('deptId');
//           }
           $good = new GoodModel();
           $res = $good->getByWhere($where, $offset, $limit);
           foreach ($res as $key => $vo) {
               $res[$key]['creattime'] = date('Y-m-d H:i:s', $vo['creattime']);
           }
           $return['total'] = $good->getAll($where);  //总数据
           $return['rows'] = $res;
           $return['sql'] = $good->getLastSql();
           return json($return);
       }else{
        

       $this->assign([
           'name'=>$name,
           'title'=>$this->title
       ]);
       return $this->fetch();
     }
    }



    public function add(){
        $name = "添加商品";
        if(request()->isPost()){
            $data = input('post.');
            $good = new GoodModel();
            $res = $good->add($data);
            if($res['code']==1){

                $this->ky_success($res['msg'],$res['data']);
            }else{

                $this->ky_error($res['msg']);
            }
        }else{
        $this->assign([
            'name'=>$name,
            'title'=>$this->title
        ]);
        return $this->fetch();
        }
    }

    public function cate(){
        return $this->fetch();
    }
}